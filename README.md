# Neovim Arch dependencies
```
pacman -S python3 python-pip neovim fzf
```
##  vim-plug

```
 curl -fLo ~/.local/share/nvim/site/autoload/plug.vim --create-dirs https://raw.githubusercontent.com/junegunn/vim-plug/master/plug.vim
```

PyNVIM
```
pip3 install --user pynvim
```

Elixir LS

```
git clone https://github.com/elixir-lsp/elixir-ls.git ~/.elixir-ls
cd ~/.elixir-ls
mix deps.get && mix compile && mix elixir_ls.release -o release
```

https://github.com/elixir-lsp/coc-elixir#server-fails-to-start

ASDF

```
git clone https://github.com/asdf-vm/asdf.git ~/.asdf --branch v0.8.0
```
